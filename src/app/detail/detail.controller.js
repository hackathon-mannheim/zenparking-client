export class DetailController {
  constructor ($scope, $timeout, $stateParams) {
    'ngInject';

    $scope.parkingName = decodeURIComponent($stateParams.name);
    $scope.totalSlots = null;

    var datasets = [];

    $.getJSON('http://herbert.lukas-bisdorf.de:8080/forecast', {}, (data) => {
      datasets = data[$scope.parkingName];
      $scope.totalSlots = data[$scope.parkingName][0].totalSlots;
      $timeout(function(){
        $scope.switchDay(0);
      });
    });

    $scope.currentIndex = 0;

    $scope.switchDay = function(index) {

      $scope.currentIndex = index;

      $scope.chartConfig.title.text = $scope.parkingName;
      $scope.chartConfig.subtitle.text = $scope.totalSlots + ' Plätzchen';

      $scope.chartConfig.xAxis.categories = [];
      var hour = 0;
      for(var i = 0; i < datasets[index].values.length; i++) {
        $scope.chartConfig.xAxis.categories.push(hour);
        hour = hour+0.5;
      }

      $scope.chartConfig.series = [];
      var values = datasets[index].values;
      var serie = [];
      var color = '';
      $.each(values, function(i, value) {
        if(value > 50) {
          color = 'green';
        } else if(value > 30) {
          color = 'orange';
        } else {
          color = 'red';
        }
        serie.push({
          y: value,
          color: color
        })
      });
      $scope.chartConfig.series.push({
        showInLegend: false,
        data: serie
      });
    }

    var defaultConfig = {
      title: {
        text: $scope.parkingName
      },
      subtitle: {
        text: ''
      },
      options: {
        chart: {
          height: '250',
          type: 'column'
        }
      },
      plotOptions: {
        column: {
          pointPadding: 0,
          pointPlacement: 'on'
        }
      },
      series: [{
        showInLegend: false,
        data: []
      }],
      xAxis: {
        categories: []
      },
      yAxis: {
        ceiling: 100,
        labels: {
          formatter: function() {
            return this.value + '%'
          }
        },
        title: {
          text: 'Parkplatz Verfügbarkeit'
        }
      },
      legend: {
        enabled: false
      }
    };

    $scope.chartConfig = defaultConfig;

    $.getJSON('http://herbert.lukas-bisdorf.de:8080/wisedom', {}, (data) => {
        $scope.wisdom = data;
    });

  }
}
