export class MainController {
  constructor ($timeout, webDevTec, toastr, $scope, leafletData, $state) {
    'ngInject';

    $scope.sliderOptions = {
      floor: 0,
      ceil: 23.5,
      step: 0.5,
      precision: 1,
      translate: (newValue) => {
        if (Math.floor(newValue) == newValue) {
          return Math.floor(newValue) + ':00';
        } else {
          return Math.floor(newValue) + ':30';
        }
      }
    }

    this.$scope = $scope;

    let now = new Date();
    let map = null;

    let fakeDays = {};

    $scope.$on('leafletDirectiveMarker.click', (e, marker) => {
      $state.go('detail', {name: encodeURIComponent(marker.model.message)});
    });

    $scope.paths = {
      markerPath: {
        color: 'red',
        weight: 3
      }
    }

    $scope.spinning = false;

    $scope.layers = {
      baselayers: {
        openStreetMap: {
          name: 'OpenStreetMap',
          type: 'xyz',
          url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        }
      },
      overlays: {
        parkingspots: {
          type: 'group',
          name: 'parkingspots',
          visible: true
        }
      }
    };

    $scope.markers = {
      location: {
        /*label: {
          message: 'Ziel',
          options: {
            noHide: true
          }
        }*/
      }
    }

    // Geocoding
    $scope.$watch('query', (newValue, oldValue) => {
      if ((!newValue && oldValue)) {
        $scope.markers.location = {};
        $scope.updateData(this.$scope.timestamp, true);
        return;
      }

      if (!newValue) return;

      console.log('Query changes', newValue);

      $scope.spinning = true;

      $.getJSON('http://nominatim.openstreetmap.org/', {
        format: 'json',
        limit: 1,
        q: newValue + ' Mannheim'
      }, (data) => {
        console.log('Geocode data');
        if (data.length) {
          this.setTargetLocation(parseFloat(data[0].lat), parseFloat(data[0].lon));
          $scope.spinning = false;
          $scope.$apply();
        }
      });
    });

    // Set hour and minutes
    $scope.time = now.getHours();

    if(now.getMinutes() > 30) {
      $scope.time += 0.5;
    }

    // @TODO: Fix date
    now.setDate(15);
    $scope.date = now;

    // Get new parking place data
    $scope.updateData = (timestamp, reset, callback) => {
      timestamp = Object.assign(timestamp);
      if (timestamp.getDate() < 10 || timestamp.getDate() > 16) {
        let timestampDay = timestamp.getDate();
        if (fakeDays[timestampDay]) {
          timestamp.setDate(fakeDays[timestampDay]);
        } else {
          timestamp.setDate(fakeDays[timestampDay] = 10 + Math.round(Math.random() * 6));
        }

        console.log('Using fake day', fakeDays[timestampDay]);
      }

      $.getJSON('http://herbert.lukas-bisdorf.de:8080/parkinglots', {
        timestamp: timestamp.getTime()
      }, (data) => {
        if (data.carparks) {

          let bounds = null;

          data.carparks.forEach((carpark, index) => {
            if (carpark.title == 'Parken an der SAP Arena' ||
                carpark.title == 'Tiefgarage D3' ||
                carpark.title == 'Tiefgarage U2') {
              // Omit SAP Arena parking decks for now they fuck up the map bounds
              // Omit Tiefgarage D3/U2 because they dont have data
              return;
            }

            let parkinghouse = {
              layer: 'parkingspots',
              message: carpark.label,
              count: carpark.count,
              icon: {
                type: 'div',
                iconSize: [30, 45],
                popupAnchor:  [0, 0]
              }
              /*label: {
                message: carpark.free ? carpark.free.toString() : '0',
                options: {
                  noHide: true
                }
              }*/
            };

            if ($scope.markers['parkinghouse' + index])
              parkinghouse = angular.merge($scope.markers['parkinghouse' + index], parkinghouse);

            if (!parkinghouse.icon.className) parkinghouse.icon.className = 'marker';

            if (reset) {
              parkinghouse.near = false;
              parkinghouse.icon.className = 'marker';
              parkinghouse.distance = null;
            }

            // Update icon template
            parkinghouse.icon.html = `
                ${parkinghouse.count >= 0 ? `<div class="marker-count">${parkinghouse.count}</div>` : ''}
                ${parkinghouse.near && parkinghouse.distance ? `<div class="marker-distance">${parkinghouse.distance}km</div>` : ''}
              `;

            if (carpark.location) {
              parkinghouse.lat = carpark.location.lat;
              parkinghouse.lng = carpark.location.lng;
            } else if (carpark.title == 'Tiefgarage U2') {
              // Tiefgarage U2 Thanks Lukas & Julian!
              parkinghouse.lat = 49.491536;
              parkinghouse.lng = 8.471888;
            } else {
              // Tiefgarage D3 Thanks Lukas!
              parkinghouse.lat = 49.4879042;
              parkinghouse.lng = 8.4635594;
            }

            if (parkinghouse.lat && parkinghouse.lng) {
              if (!bounds) {
                bounds = new L.latLngBounds([parkinghouse.lat, parkinghouse.lng]);
              } else {
                bounds.extend([parkinghouse.lat, parkinghouse.lng]);
              }
            }

            $scope.markers['parkinghouse' + index] = parkinghouse;
          });

          $scope.$apply();

          if (callback) callback.call();

          if (this.map && !$scope.query) {
            // Update bounds after new marker data
            this.map.fitBounds(bounds.pad(0.05));
          }
        }
      });
    };

    // Update parking place data on date/timechange
    $scope.$watchGroup(['date', 'time'], (newValues, oldValues) => {
      let timestamp = newValues[0];
      let hour = Math.floor(newValues[1]);
      timestamp.setHours(hour);
      if (newValues[1] > hour) {
        timestamp.setMinutes(30);
      }

      this.$scope.timestamp = timestamp;

      $scope.updateData(timestamp, false, () => {
        if ($scope.markers.location.lat) {
          this.setTargetLocation($scope.markers.location.lat, $scope.markers.location.lng)
        }
      });
    });

    // Get device position
    $scope.getPosition = () => {
      if (navigator.geolocation) {
        $scope.spinning = true;
        navigator.geolocation.getCurrentPosition((pos) => {
          this.setTargetLocation(pos.coords.latitude, pos.coords.longitude);
          $scope.query = '';
          $scope.spinning = false;
          $scope.$apply();
        });
      }
    };

    // Make map available
    leafletData.getMap().then((newMap) => {
      this.map = newMap;

      this.$scope.timestamp = now;

      $scope.updateData(now);
    });
  }

  //
  setTargetLocation(lat, lng) {
    this.$scope.markers.location.lat = lat;
    this.$scope.markers.location.lng = lng;

    let markers = [];

    for (let key of Object.keys(this.$scope.markers)) {
      let marker = this.$scope.markers[key];
      if (key.match(/^parkinghouse/)) {
        marker.near = false;
        marker.icon.className = 'marker';
        marker.distance = this.latLngDistance(lat, lng, marker.lat, marker.lng).toFixed(2);
        marker.score = marker.distance / Math.sqrt(marker.count);
        marker.icon.html = `
                  ${marker.count ? `<div class="marker-count">${marker.count}</div>` : ''}
                  ${marker.distance && marker.near ? `<div class="marker-distance">${marker.distance}km</div>` : ''}
                `;

        markers.push(marker);
      }
    }

    let sortedMarkers = markers.sort((a, b) => a.score < b.score ? -1 : 1);

    let bounds = L.latLngBounds([lat, lng]);
    bounds.extend([lat, lng]);

    this.$scope.nearParkingPlaces = sortedMarkers.slice(0, 3);

    this.$scope.nearParkingPlaces.forEach((place) => {
      place.near = true;
      place.icon.className = 'marker near';
      place.icon.html = `
                  ${place.count >= 0 ? `<div class="marker-count">${place.count}</div>` : ''}
                  ${place.distance && place.near ? `<div class="marker-distance">${place.distance}km</div>` : ''}
                `;
      bounds.extend([place.lat, place.lng])
    });

    if (this.map) {
      // Update bounds after location update
      this.map.fitBounds(bounds.pad(0.05));
    }
  }

  //
  latLngDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;    // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p))/2;

    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  }
}
