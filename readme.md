# ZenParking

With this app you can check the status of parking lots, at any given time! You also get historical data as well as fun facts about out beautiful city Mannheim

## Setup

```
$ npm install -g bower gulp
$ npm install
$ bower install
```

`$ gulp serve` to run it locally

`$ gulp build` to build a distribution version
